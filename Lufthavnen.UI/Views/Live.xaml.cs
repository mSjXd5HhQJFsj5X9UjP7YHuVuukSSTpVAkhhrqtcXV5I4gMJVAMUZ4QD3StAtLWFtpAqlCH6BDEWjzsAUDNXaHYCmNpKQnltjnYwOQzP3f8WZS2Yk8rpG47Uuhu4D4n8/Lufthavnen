﻿using System;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using Lufthavnen.Core.Events;
using Lufthavnen.Core.Machines;
using Lufthavnen.Core.Objects;

namespace Lufthavnen.UI.Views {
	public partial class Live {
		public Live() {
			InitializeComponent();

			BufferBlock<Luggage> buffer = new();
			BufferBlock<Luggage> sortedBuffer = new();
			Desk d = new();
			Sorter s = new();
			s.SortEvent += (o, e) => Task.Run(async () => await SortEvent(o, e));
			Plane p = new();

			s.Start(buffer, sortedBuffer);
			d.Start(buffer);
		}

		private async Task SortEvent(object source, ObjectEventArgs<Luggage> e) {
			await Application.Current.Dispatcher.BeginInvoke((Action)(() => {
				LiveView.Children.Add(e.GetObject());
				e.GetObject().MoveX(1100, 2);
			}));
		}
	}
}