﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Lufthavnen.Core.Graphics;
using Lufthavnen.Core.Machines;
using Lufthavnen.Core.Objects;
using Lufthavnen.UI.Views;

namespace Lufthavnen.UI {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		private object _content;

		public object Content {
			get => _content;
			set => _content = value;
		}

		public MainWindow() {
			InitializeComponent();
			Live live = new Live();
			DataContext = live;
		}
	}
}