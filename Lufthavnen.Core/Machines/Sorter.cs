﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Lufthavnen.Core.Events;
using Lufthavnen.Core.Objects;

namespace Lufthavnen.Core.Machines {
	public class Sorter {
		public ChangeEvent<Luggage> SortEvent;

		public async Task Start(ISourceBlock<Luggage> source, ITargetBlock<Luggage> target) {
			await Task.Run(async () => await Sort(source, target));
		}

		private async Task Sort(ISourceBlock<Luggage> source, ITargetBlock<Luggage> target) {
			while (await source.OutputAvailableAsync()){
				await source.ReceiveAsync().ContinueWith(x => {
					target.Post(x.Result);
					SortEvent(this, new ObjectEventArgs<Luggage>(x.Result));
					Debug.Print($"Thread={Thread.CurrentThread.ManagedThreadId} sorted luggage");
				});
			}
		}
	}
}