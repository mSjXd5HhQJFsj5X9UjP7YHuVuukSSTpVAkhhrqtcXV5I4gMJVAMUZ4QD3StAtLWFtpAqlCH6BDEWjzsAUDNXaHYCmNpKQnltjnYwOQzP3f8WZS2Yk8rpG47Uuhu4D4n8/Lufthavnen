﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using System.Windows.Controls;
using Lufthavnen.Core.Events;
using Lufthavnen.Core.Graphics;
using Lufthavnen.Core.Objects;

namespace Lufthavnen.Core.Machines {
	public class Plane {
		public event ConsumeEvent<Luggage> OnConsume;

		public Plane() {
		}

		public void Take() {
			OnConsume(this, null);
		}
	}
}