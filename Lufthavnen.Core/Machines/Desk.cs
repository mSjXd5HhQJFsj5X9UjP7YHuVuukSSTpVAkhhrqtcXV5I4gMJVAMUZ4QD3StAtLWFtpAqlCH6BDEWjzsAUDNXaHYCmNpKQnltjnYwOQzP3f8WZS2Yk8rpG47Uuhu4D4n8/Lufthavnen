﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using Lufthavnen.Core.Graphics;
using Lufthavnen.Core.Objects;
using Timer = System.Timers.Timer;

namespace Lufthavnen.Core.Machines {
	public class Desk {
		public void Start(ITargetBlock<Luggage> target) {
			// Set the Interval to 1 seconds (1000 ms).
			Timer timer = new(1000);
			timer.Elapsed += async (_, _) => await Task.Run(async () => { await Produce(target); });
			// Run indefinitely
			timer.Enabled = true;
		}

		private async Task Produce(ITargetBlock<Luggage> target) {
			await Application.Current.Dispatcher.BeginInvoke((Action)(() => {
				target.Post(new Luggage(new Position(0, 200)));
			}));
			Debug.Print($"Thread={Thread.CurrentThread.ManagedThreadId} produced luggage");
			//target.Complete();
		}
	}
}