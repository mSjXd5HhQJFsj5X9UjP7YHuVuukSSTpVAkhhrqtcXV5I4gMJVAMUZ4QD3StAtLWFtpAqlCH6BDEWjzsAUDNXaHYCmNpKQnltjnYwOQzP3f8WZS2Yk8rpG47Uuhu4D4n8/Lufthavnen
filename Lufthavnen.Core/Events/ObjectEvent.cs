﻿using System;

namespace Lufthavnen.Core.Events {
	public delegate void ChangeEvent<T>(object source, ObjectEventArgs<T> e);

	public delegate void ProduceEvent<T>(object source, ObjectEventArgs<T> e);

	public delegate void ConsumeEvent<T>(object source, ObjectEventArgs<T> e);
	
	public class ObjectEventArgs<T> : EventArgs {
		private readonly T _eventObject;

		public ObjectEventArgs(T o) {
			_eventObject = o;
		}

		public T GetObject() {
			return _eventObject;
		}
	}
}