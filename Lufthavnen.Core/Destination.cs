﻿namespace Lufthavnen.Core {
	// IATA airport codes
	public enum Destination {
		CPH,
		AAL,
		LAX
	}
}