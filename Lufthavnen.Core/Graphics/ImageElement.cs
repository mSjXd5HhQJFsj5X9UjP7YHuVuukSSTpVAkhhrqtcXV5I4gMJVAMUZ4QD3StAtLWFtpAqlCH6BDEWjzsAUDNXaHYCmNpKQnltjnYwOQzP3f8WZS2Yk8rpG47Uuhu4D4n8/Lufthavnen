using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Lufthavnen.Core.Graphics {
	public class ImageElement : Image {
		private readonly Storyboard _storyboard = new();
		public ImageElement() { }

		public ImageElement(ImageSource source, Position pos) {
			Source = source;
			Pos = pos;
		}

		public Position Pos {
			get => new(Canvas.GetLeft(this), Canvas.GetTop(this));
			set {
				Canvas.SetLeft(this, value?.X ?? 0);
				Canvas.SetTop(this, value?.Y ?? 0);
			}
		}

		/// <summary>
		///     Logic for smoothly moving a Bitmap in the X axis
		/// </summary>
		public void MoveX(double posX, double duration) {
			Storyboard storyboard = new();
			DoubleAnimation anim = new(Pos.X, posX, new Duration(TimeSpan.FromSeconds(duration)));

			Storyboard.SetTarget(anim, this);
			Storyboard.SetTargetProperty(anim, new PropertyPath(Canvas.LeftProperty));
			storyboard.Children.Add(anim);
			storyboard.Begin();
			Pos.X = posX;
		}

		/// <summary>
		///     Logic for smoothly moving a Bitmap in the Y axis
		/// </summary>
		public void MoveY(double posY, double duration) {
			Storyboard storyboard = new();
			DoubleAnimation anim = new(Pos.Y, posY, new Duration(TimeSpan.FromSeconds(duration)));

			Storyboard.SetTarget(anim, this);
			Storyboard.SetTargetProperty(anim, new PropertyPath(Canvas.TopProperty));
			storyboard.Children.Add(anim);
			storyboard.Begin();
			Pos.Y = posY;
		}

		/// <summary>
		///     Logic for smoothly moving a Bitmap
		/// </summary>
		public void Move(Position newPos, double duration) {
			MoveX(newPos.X, duration);
			MoveY(newPos.Y, duration);
		}

		public void MoveSync(Position newPos, double duration) {
			_storyboard.Completed -= (sender, args) => { MoveY(newPos.Y, duration); };
			MoveX(newPos.X, duration);
		}

		/// <summary>
		///     Scale image size by input
		/// </summary>
		public void SetScale(double scale) {
			Width *= scale;
			Height *= scale;
		}
	}
}