﻿namespace Lufthavnen.Core.Graphics {
	public class Position {
		private double _posX = 0;
		private double _posY = 0;

		public Position(double? posX, double? posY) {
			X = posX ?? 0;
			Y = posY ?? 0;
		}

		public double X {
			get => _posX;
			set => _posX = value;
		}

		public double Y {
			get => _posY;
			set => _posY = value;
		}
	}
}