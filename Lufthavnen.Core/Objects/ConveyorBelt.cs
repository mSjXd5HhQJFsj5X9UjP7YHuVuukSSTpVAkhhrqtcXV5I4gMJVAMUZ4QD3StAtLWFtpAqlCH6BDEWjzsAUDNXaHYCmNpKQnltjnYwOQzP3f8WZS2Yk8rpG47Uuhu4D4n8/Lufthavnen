﻿using System;
using System.Windows.Media.Imaging;
using Lufthavnen.Core.Graphics;

namespace Lufthavnen.Core.Objects {
	public class ConveyorBelt : ImageElement {
	
		public ConveyorBelt() {
			Source = new BitmapImage(
				new Uri("pack://application:,,,/Lufthavnen.UI;component/Resources/conveyor_top.png"));
		}
	}
}