﻿using System;
using System.Windows.Media.Imaging;
using Lufthavnen.Core.Graphics;

namespace Lufthavnen.Core.Objects {
	public class Luggage : ImageElement {
		public Luggage(Position? pos = null) {
			Source = new BitmapImage(
				new Uri("pack://application:,,,/Lufthavnen.UI;component/Resources/luggage.png"));
			Pos = pos;
		}
	}
}